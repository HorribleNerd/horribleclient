/*
 * Copyright 2018 ImpactDevelopment
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.horriblenerd.horribleclient.mod;

import clientapi.ClientAPI;
import clientapi.manage.Manager;
import clientapi.module.Module;
import com.horriblenerd.horribleclient.mod.mods.Aura;
import com.horriblenerd.horribleclient.mod.mods.Fly;
import com.horriblenerd.horribleclient.mod.mods.Hud;
import com.horriblenerd.horribleclient.mod.mods.Speed;
import com.horriblenerd.horribleclient.mod.mods.Gamma;

public final class ModManager extends Manager<Module> {

    public ModManager() {
        super("Module");
    }

    @Override
    public final void load() {
        ClientAPI.LOGGER.info("Loading Modules");

        // Load Modules
        this.addAll(
            new Aura(),
            new Fly(),
            new Hud(),
            new Speed(),
            new Gamma()
        );
    }

    @Override
    public final void save() {
        // Save Modules
    }
}
