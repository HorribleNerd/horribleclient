/*
 * Copyright 2018 ImpactDevelopment
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.horriblenerd.horribleclient.mod.mods;

import clientapi.module.Module;
import clientapi.module.annotation.Mod;
import net.minecraft.client.settings.GameSettings;
import org.lwjgl.input.Keyboard;

@Mod(name = "Gamma", description = "A basic gamma module", bind = Keyboard.KEY_N)
public final class Gamma extends Module {
    GameSettings settings = mc.gameSettings;
    float oldGamma;

    public Gamma() {
        oldGamma = settings.gammaSetting;
    }

    @Override
    public void toggle() {
        if (oldGamma == settings.gammaSetting) {
            this.setState(true);
            settings.gammaSetting = 100.0f;
        }
        else {
            this.setState(false);
            settings.gammaSetting = oldGamma;
        }
    }
}
