/*
 * Copyright 2018 ImpactDevelopment
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.horriblenerd.horribleclient.mod.mods;

import clientapi.event.defaults.game.render.RenderHudEvent;
import clientapi.module.Module;
import clientapi.module.annotation.Mod;
import com.horriblenerd.horribleclient.HorribleClient;
import com.horriblenerd.horribleclient.mod.category.IRender;
import me.zero.alpine.listener.EventHandler;
import me.zero.alpine.listener.Listener;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.ScaledResolution;

import java.util.Comparator;

@Mod(name = "HUD", description = "Displays an In-Game HUD")
public final class Hud extends Module implements IRender {

    private int y;

    public Hud() {
        // Force Hud to enable on start
        this.setState(true);
    }

    @EventHandler
    private Listener<RenderHudEvent> render2DListener = new Listener<>(event -> {
        ScaledResolution sr = event.getScaledResolution();
        FontRenderer font = mc.fontRenderer;

        int color = 0xFF98ABB8;

        font.drawStringWithShadow("HorribleClient", 2, 2, color);

        y = sr.getScaledHeight() - 12;

        HorribleClient.getInstance().getModuleManager().stream().filter(Module::getState)
                .sorted(Comparator.comparingInt(m -> -font.getStringWidth(m.getName()))).forEach(module -> {
            font.drawStringWithShadow(module.getName(), sr.getScaledWidth() - 2 - font.getStringWidth(module.getName()), y, color);
            y -= font.FONT_HEIGHT;
        });
    });
}
